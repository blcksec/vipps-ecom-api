<!-- START_METADATA
---
draft: true
---
END_METADATA -->

# Outdated information

The PDFs in this directory are severely outdated, and provided for reference only.

See the [current documentation](../vipps-ecom-api.md).
